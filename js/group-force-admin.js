(function($) {
  'use strict';
  Drupal.behaviors.h4c_viewtoggle = {
    attach: function (context, settings) {
      // TODO: Do this properly in code
      // prop('readonly',true) does not work anymore (in gin?)
      $('#edit-group-roles-h4c-organisation-group-admin').prop('checked','true').css({ "pointer-events": "none" });
      $('label[for="edit-group-roles-h4c-organisation-group-admin"]').css({ "pointer-events": "none" });
      
      $('#edit-group-roles-h4c-editorial-group-admin').prop('checked','true').css({ "pointer-events": "none" });
      $('label[for="edit-group-roles-h4c-editorial-group-admin"]').css({ "pointer-events": "none" });

      $('#edit-group-roles-h4c-campaign-group-admin').prop('checked','true').css({ "pointer-events": "none" });
      $('label[for="edit-group-roles-h4c-campaign-group-admin"]').css({ "pointer-events": "none" });

  }};
})(jQuery);
